variable "vpc_cidr_block" {
  default = "192.168.0.0/16"
}

variable "region" {
  default = "us-west-2"
}
