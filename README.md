# Tagnos Terraform

This is a general guide on how to use this Terraform code for creating
* VPC
* Security Groups
* VPN Instance
* ALB
* Mysql RDS, etc

### Getting terraform
1. Visit: https://www.terraform.io/downloads.html
2. Choose your respective OS. (We are using Ubuntu)
3. Download 64-bit version
4. unzip the downloaded file
5. Move the `terraform` binary to `/usr/bin/terraform`

The scripts are configured to use S3 for storing `terraform.tfstate` file.

### How to use it
1. **Create the S3 bucket and DynamoDB table** <br />
   As mentioned above we are creating S3 to store state file.<br />
   DynamoDB table is used to create locking.<br />
   It is necessary to have locking because state file can be used by multiple user and could result in corrupting it.
    
   ```
   cd global/s3
   terraform init
   terraform validate
   terraform apply
   ```
2. **Create VPC for respective environment** <br />
   Move in `dev_vpc`, `mgmt_vpc` and `prod_vpc` folders as you need and create VPC's <br />
   VPC will be required by every resource and therefore we will store its state on S3. <br />
   Terraform script will take care of placing the state file for future referencing in S3.
   ```
   cd ../prod_vpc
   terraform init
   terraform validate
   terraform apply
   ``` 
   **Note**: Apply the same to `dev_vpc` and `mgmt_vpc`
   
3. **Create openVPN server** <br />
   OpenVPN server will be required to connect to all the instances that will be created in the VPC.
   Because all of them would be in private IP subnet
   ```
   cd ../prod_openvpn/
   terraform init
   terraform validate
   terraform apply
   ```
   **Note**: Apply the same to `dev_openvpn` and `mgmt_openvpn`

4. **Create peering VPC peering** <br />
   We will have VPC peering between `mgmt and dev` and `mgmt and prod` and no routing between `dev and prod` <br />
   This is for security purpose. Services like Jenkins which will reside in `mgmt VPC` and will be able to access dev and prod when needed.
   ```
   cd ../vpc_peering/
   terraform init
   terraform validate
   terraform apply
   ``` 

5. **Create Prod MySQL RDS** <br />
   It will be using values as defined in `vars.tf`. It can be changed if needed. <br />
   It will ask for password after apply. Password has to be atleast 8 character long.
   ```
   cd ../prod_mysql/
   terraform init
   terraform validate
   terraform apply
   < Insert pasword for MySQL >
   ```
   If you do not want to keep providing password everytime than you can create a `terraform.tfvars` and insert `mysql_password = "<your password>"`. An example of this usage is done when creating wildfly server.

6. **Create Prod Wildfly server**
   Wildfly server when gets created will have Erlang and RabbitMQ installed. It does not have wildfly as of now. <br />
   Kumar/Tagnos have code in github which is used to install wildfly. That code will be needed to be inserted in userdata to have that installed as well. <br />
   As explained in point 5. `wildfly` has `.tfvars` file so it will consume the password mentioned in that file. 
   ```
   cd ../prod_wildfly/
   terraform init
   terraform validate
   terraform apply
   ```
   
7. **Create Prod ALB**
   Here we will be creating application load balancer. This will also take care of creating target group and assigning the wildfly instance to it. <br />
   ```
   cd ../prod_alb/
   terraform init
   terraform validate
   terraform apply
   ``` 