variable "engine_version" {
  default = "5.6.44"
}
variable "instance_class" {
  default = "db.t2.large"
}
variable "username" {
  default = "test"
}

variable "mysql_password" {
  type = string
}

variable "region" {
  default = "us-west-2"
}
