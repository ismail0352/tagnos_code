provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "tagnos"
}

module "backend_terraform_state" {
  source  = "../modules/backend_terraform_state"
  key     = "global/s3/prod/vpc/terraform.tfstate"
  region  = var.region
}

module "prod-mysql-sg" {
  source                    = "../modules/security-group"
  name                      = "prod-mysql-sg"
  description               = "Secuirty Group for mysql rds"
  vpc_id                    = module.backend_terraform_state.vpc_id
  ingress_cidr_blocks       = ["0.0.0.0/0"]
  ingress_ipv6_cidr_blocks  = ["::/0"]
  ingress_rules             = ["ssh-tcp", "http-80-tcp", "https-443-tcp", "mysql-tcp"]
  egress_rules              = ["all-all"]

  tags = {
    Owner              = "Tagnos"
  }
}

module "db" {
  source = "../modules/rds"

  identifier = "prod-mysql-db"

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine            = "mysql"
  engine_version    = var.engine_version //"5.6.44"
  instance_class    = var.instance_class //"db.t2.large"
  allocated_storage = 5
  storage_encrypted = false

  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name     = "prod_mysql_db"
  username = var.username
  password = var.mysql_password
  port     = 3306

  vpc_security_group_ids = [module.prod-mysql-sg.this_security_group_id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  multi_az = true

  # disable backups to create DB faster
  backup_retention_period = 0

  tags = {
    Owner       = "Tagnos"
    Environment = "prod"
  }

  # DB subnet group
//  subnet_ids = tolist(data.terraform_remote_state.vpc.outputs.private_subnet_ids)
  subnet_ids = tolist(module.backend_terraform_state.private_subnet_ids)

  # DB parameter group
  family = "mysql5.6"

  # DB option group
  major_engine_version = "5.6"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "prod-mysql-db"

  # Database Deletion Protection
  deletion_protection = false

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8"
    },
    {
      name  = "character_set_server"
      value = "utf8"
    }
  ]

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]
}
