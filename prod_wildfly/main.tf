provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "tagnos"
}

module "backend_terraform_state" {
  source  = "../modules/backend_terraform_state"
  key     = "global/s3/prod/vpc/terraform.tfstate"
  region  = var.region
}

module "prod-wildfly-sg" {
  source                    = "../modules/security-group"
  name                      = "prod-wildfly-sg"
  description               = "Secuirty Group for prod-wildfly server"
  vpc_id                    = module.backend_terraform_state.vpc_id
  ingress_cidr_blocks       = ["0.0.0.0/0"]
  ingress_ipv6_cidr_blocks  = ["::/0"]
  ingress_rules             = ["ssh-tcp", "http-80-tcp", "https-443-tcp", "http-8080-tcp"]
  egress_rules              = ["all-all"]

  tags = {
    Owner                   = "Tagnos"
  }
}

// To use below data resource Run terraform command as:
// `tf12 apply -target=aws_ami.ubuntu18`
// `tf12 apply`
// Got fixed in version:
// Terraform v0.12.16
// + provider.aws v2.36.0
// + provider.template v2.1.2

data "aws_ami" "ubuntu18" {
  most_recent = true
  owners = [var.ubuntu_account_number]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}

data "template_file" "wildfly_user_data" {
  template = file("./wildfly.tpl")
  vars = {
    rabbitmq_password = var.rabbitmq_password
  }
}

module "ec2" {
  source = "../modules/ec2-instance"
  instance_count              = 2
  name                        = "prod-wildfly-server"
  ami                         = data.aws_ami.ubuntu18.id
  instance_type               = var.instance_type
  subnet_id                   = tolist(module.backend_terraform_state.private_subnet_ids)[0]
  vpc_security_group_ids      = [module.prod-wildfly-sg.this_security_group_id]
  associate_public_ip_address = false
  key_name                    = var.key_pair
  user_data                   = data.template_file.wildfly_user_data.rendered

  tags = {
    Owner = "Tagnos"
  }
}

// For local
//terraform {
//  backend "local" {
//    path = "/opt/tagnos/prod/instance/terraform.tfstate"
//  }
//}

terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "tagnos-terraform-backend-state"
    key            = "global/s3/prod/instance/terraform.tfstate"
    region         = "us-west-2"
    # Replace this with your DynamoDB table name!
    dynamodb_table = "tagnos-terraform-backend-locks"
    encrypt        = true
    # Without profile it fails -> probably it takes default profile
    profile        = "tagnos"
  }
}
