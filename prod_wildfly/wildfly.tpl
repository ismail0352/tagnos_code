#!/usr/bin/env bash

# Install Erlang for RabbitMQ
wget -O- https://packages.erlang-solutions.com/ubuntu/erlang_solutions.asc | sudo apt-key add -
echo "deb https://packages.erlang-solutions.com/ubuntu bionic contrib" | sudo tee /etc/apt/sources.list.d/rabbitmq.list

sudo apt update
sudo apt -y install erlang

# Dependencies for RabbitMQ
sudo apt-get -y install socat logrotate init-system-helpers adduser

# Installing RabbitMQ from source
sudo apt updatewget -O- https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc | sudo apt-key add -
wget https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.8.1/rabbitmq-server_3.8.1-1_all.deb
sudo dpkg -i rabbitmq-server_3.8.1-1_all.deb

sudo rabbitmqctl add_user test '${rabbitmq_password}'
sudo rabbitmqctl set_user_tags test administrator
