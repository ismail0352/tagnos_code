output "peering_id_mgmt_prod" {
  value = module.vpc_peering_mgmt_prod.vpc_peering_id
}

output "peering_id_mgmt_dev" {
  value = module.vpc_peering_mgmt_dev.vpc_peering_id
}
