provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "tagnos"
}

module "backend_terraform_state_mgmt_vpc" {
  source  = "../modules/backend_terraform_state"
  key     = "global/s3/mgmt/vpc/terraform.tfstate"
  region  = var.region
}

module "backend_terraform_state_prod_vpc" {
  source  = "../modules/backend_terraform_state"
  key     = "global/s3/prod/vpc/terraform.tfstate"
  region  = var.region
}

module "backend_terraform_state_dev_vpc" {
  source  = "../modules/backend_terraform_state"
  key     = "global/s3/dev/vpc/terraform.tfstate"
  region  = var.region
}

module "vpc_peering_mgmt_prod" {
  source = "../modules/vpc-peering"

  providers = {
    aws.this = aws
    aws.peer = aws
  }

  this_vpc_id = module.backend_terraform_state_mgmt_vpc.vpc_id
  peer_vpc_id = module.backend_terraform_state_prod_vpc.vpc_id

  auto_accept_peering = true

  tags = {
    Name        = "Managment to Production"
  }
}

module "vpc_peering_mgmt_dev" {
  source = "../modules/vpc-peering"

  providers = {
    aws.this = aws
    aws.peer = aws
  }

  this_vpc_id = module.backend_terraform_state_mgmt_vpc.vpc_id
  peer_vpc_id = module.backend_terraform_state_dev_vpc.vpc_id

  auto_accept_peering = true

  tags = {
    Name        = "Managment to Development"
  }
}
