variable "region" {
  default = "us-west-2"
}

variable "ubuntu_account_number" {
  default = "099720109477"
}

variable "instance_type" {
  default = "t3.large"
}

variable "key_pair" {
  default = "tagnos"
}

variable "ami_id" {
  description = "Ubuntu 18 ami_id"
  default     = "ami-00622b440d92e55c0"
}
