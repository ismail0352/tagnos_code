provider "aws" {
  region                  = "us-west-2"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "tagnos"
}

data "terraform_remote_state" "vpc" {
  backend             = "local"
  config = {
    path              = "/opt/tagnos/mgmt/terraform.tfstate"
  }
}

module "jenkins-sg" {
  source                    = "../modules/security-group"
  name                      = "jenkins-sg"
  description               = "Secuirty Group for jenkins server (Launch Wizard 10)"
  vpc_id                    = data.terraform_remote_state.vpc.outputs.vpc_id
  ingress_cidr_blocks       = ["0.0.0.0/0"]
//  ingress_ipv6_cidr_blocks  = ["::/0"]
  ingress_rules             = ["ssh-tcp",]
  ingress_with_cidr_blocks = [
    {
      from_port   = 9090
      to_port     = 9090
      protocol    = "tcp"
      description = "Jenkins machine"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  egress_rules              = ["all-all"]

  tags = {
    Owner                   = "Tagnos"
  }
}

data "aws_ami" "ubuntu18" {
  most_recent = true
  owners = [var.ubuntu_account_number]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}

module "ec2" {
  source = "../modules/ec2-instance"
  name                        = "jenkins-server"
  ami                         = data.aws_ami.ubuntu18.id
  instance_type               = var.instance_type
  subnet_id                   = tolist(data.terraform_remote_state.vpc.outputs.private_subnet_ids)[0]
  vpc_security_group_ids      = [module.jenkins-sg.this_security_group_id]
  associate_public_ip_address = true
  key_name                    = var.key_pair


  tags = {
    Owner = "Tagnos"
  }
}
