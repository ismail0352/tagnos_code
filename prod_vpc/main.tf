provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "tagnos"
}

module "vpc" {
  source = "../modules/vpc"

  name = "Production-VPC"

  cidr = var.vpc_cidr_block # 172.16.0.0/16

  azs             = ["us-west-2a", "us-west-2b"]
  private_subnets = ["172.16.1.0/24", "172.16.2.0/24"]
  public_subnets  = ["172.16.101.0/24", "172.16.102.0/24"]


  enable_dns_hostnames = true
  enable_dns_support   = true

//  Uncomment below line to enable ipv6
//  enable_ipv6 = true

  enable_nat_gateway = true
  single_nat_gateway = true

  tags = {
    Owner       = "Tagnos"
    Environment = "prod"
  }
}

// For local
//terraform {
//  backend "local" {
//    path = "/opt/tagnos/prod/terraform.tfstate"
//  }
//}

terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "tagnos-terraform-backend-state"
    key            = "global/s3/prod/vpc/terraform.tfstate"
    region         = "us-west-2"
    # Replace this with your DynamoDB table name!
    dynamodb_table = "tagnos-terraform-backend-locks"
    encrypt        = true
    # Without profile it fails -> probably it takes default profile
    profile        = "tagnos"
  }
}
