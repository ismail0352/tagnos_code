variable "vpc_cidr_block" {
  default = "172.16.0.0/16"
}

variable "region" {
  default = "us-west-2"
}
