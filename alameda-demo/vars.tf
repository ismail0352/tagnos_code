variable "region" {
  default = "us-west-2"
}

variable "ubuntu_account_number" {
  default = "099720109477"
}

variable "instance_type" {
  default = "t3.large"
}

variable "key_pair" {
  default = "tagnos"
}
