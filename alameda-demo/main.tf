provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path              = "/opt/tagnos/dev/terraform.tfstate"
  }
}

module "sg-groups" {
  source                    = "../modules/security-group"
  name                      = "alameda-demo-sg"
  description               = "Secuirty Group for alameda-demo server"
  vpc_id                    = data.terraform_remote_state.vpc.outputs.vpc_id
  ingress_cidr_blocks       = ["0.0.0.0/0"]
  ingress_ipv6_cidr_blocks  = ["::/0"]
  ingress_rules             = ["all-all"]
  egress_rules              = ["all-all"]

  tags = {
    Owner                   = "Tagnos"
  }
}

data "aws_ami" "ubuntu18" {
  most_recent = true
  owners = [var.ubuntu_account_number]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}

module "ec2" {
  source = "../modules/ec2-instance"
  name                        = "alameda-demo"
  ami                         = data.aws_ami.ubuntu18.id
  instance_type               = var.instance_type
  subnet_id                   = tolist(data.terraform_remote_state.vpc.outputs.private_subnet_ids)[0]
  vpc_security_group_ids      = [module.sg-groups.this_security_group_id]
  associate_public_ip_address = false
  key_name                    = var.key_pair

  tags = {
    Owner = "Tagnos"
  }
}