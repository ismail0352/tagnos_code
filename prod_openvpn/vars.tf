variable "region" {
  default = "us-west-2"
}

variable "ubuntu_account_number" {
  default = "099720109477"
}

variable "instance_type" {
  default = "t3.micro"
}

variable "key_pair" {
  default = "tagnos"
}

variable "destination_path" {
  description = "Path where the file will be placed in AWS instance"
  type        = string
}

variable "source_path" {
  description = "Path where the file is on Terraform machine"
  type        = string
}

variable "ssh_user" {
  description = "ssh user for making connection using provisioner"
  type        = string
  default     = "ubuntu"
}

variable "ssh_key" {
  description = "ssh key for making connection using provisioner"
  type        = string
  default     = "tagnos.pem"
}

variable "ami_id" {
  description = "Ubuntu 18 ami_id"
  default     = "ami-00622b440d92e55c0"
}
