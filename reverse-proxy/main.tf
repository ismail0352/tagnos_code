provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path              = "/opt/tagnos/dev/terraform.tfstate"
  }
}

module "reverse-proxy-sg" {
  source                    = "../modules/security-group"
  name                      = "reverse-proxy-sg"
  description               = "Secuirty Group for reverse-proxy server"
  vpc_id                    = data.terraform_remote_state.vpc.outputs.vpc_id
  ingress_cidr_blocks       = ["0.0.0.0/0"]
  ingress_ipv6_cidr_blocks  = ["::/0"]
  ingress_rules             = ["ssh-tcp", "http-80-tcp"]
  egress_rules              = ["all-all"]

  tags = {
    Owner                   = "Tagnos"
  }
}

data "aws_ami" "ubuntu18" {
  most_recent = true
  owners = [var.ubuntu_account_number]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}

module "ec2" {
  source = "../modules/ec2-instance"
  name                        = "reverse-proxy-server"
  ami                         = data.aws_ami.ubuntu18.id
  instance_type               = var.instance_type
  subnet_id                   = tolist(data.terraform_remote_state.vpc.outputs.private_subnet_ids)[0]
  //  private_ips                 = ["172.31.32.5", "172.31.46.20"]
  vpc_security_group_ids      = [module.reverse-proxy-sg.this_security_group_id]
  associate_public_ip_address = true
  key_name                    = var.key_pair

  tags = {
    Owner = "Tagnos"
  }
}