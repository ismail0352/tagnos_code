provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path              = "/opt/tagnos/dev/terraform.tfstate"
  }
}

module "win-ops-1-sg" {
  source = "../modules/security-group"

  name = "win-ops-1-sg"
  description = "Security group for win-ops-1 (represents launch-wizard-15)"
  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id

  ingress_cidr_blocks       = ["0.0.0.0/0"]
  ingress_ipv6_cidr_blocks  = ["::/0"]
  ingress_rules             = ["rdp-tcp", "mssql-tcp"]

  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = -1
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  tags = {
    Name = "Win_Ops_1_Security_Groups"
    Owner = "Tagnos"
  }
}

data "aws_ami" "WindowsOperations" {
  most_recent = true
  owners = ["self"]

  filter {
    name   = "name"
    values = ["Windows Operations 1*"]
  }
}

module "ec2" {
  source = "../modules/ec2-instance"
  name                        = "zebra-demo-wildfly"
  ami                         = data.aws_ami.WindowsOperations.id
  instance_type               = var.instance_type
  subnet_id                   = tolist(data.terraform_remote_state.vpc.outputs.private_subnet_ids)[0]
  vpc_security_group_ids      = [module.win-ops-1-sg.this_security_group_id]
  associate_public_ip_address = false
  key_name                    = var.key_pair

  tags = {
    Owner = "Tagnos"
  }
}