variable "region" {
  default = "us-west-2"
}

variable "instance_type" {
  default = "t3.large"
}

variable "key_pair" {
  default = "tagnos"
}
