#!/bin/bash
sudo su
yum update -y
/bin/sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
useradd -d /home/srie -s /bin/bash -p '$6$89HaS0Kk$7C1E.51IsKvCad82fpdmVuaCqtVkSzZu82JW89RMXeO98jzAuHE/KqGj899/LavShXrfG1HHmHf65ZwetZjfc/' srie
echo "srie ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/cloud-init
service sshd restart
