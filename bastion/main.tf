provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path              = "/opt/tagnos/mgmt/terraform.tfstate"
  }
}

module "sg-groups" {
  source                    = "../modules/security-group"
  name                      = "bastion-sg"
  description               = "Secuirty Group for bastion server"
  vpc_id                    = data.terraform_remote_state.vpc.outputs.vpc_id
  ingress_cidr_blocks       = ["0.0.0.0/0"]
//  ingress_ipv6_cidr_blocks  = ["::/0"]
  ingress_rules             = ["ssh-tcp"]
  egress_rules              = ["all-all"]

  tags = {
    Owner                   = "Tagnos"
  }
}

data "aws_ami" "amazonlinux2" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0*x86_64**"]
  }
}

data "template_file" "bastion_user_data" {
  template = file("./bastion.tpl")
}

module "ec2" {
  source = "../modules/ec2-instance"
  name                        = "bastion"
  ami                         = data.aws_ami.amazonlinux2.id
  instance_type               = var.instance_type
  subnet_id                   = tolist(data.terraform_remote_state.vpc.outputs.private_subnet_ids)[0]
  vpc_security_group_ids      = [module.sg-groups.this_security_group_id]
  associate_public_ip_address = true
  key_name                    = var.key_pair
  user_data                   = data.template_file.bastion_user_data.rendered

  tags = {
    Owner = "Tagnos"
  }
}