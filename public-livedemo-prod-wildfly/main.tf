provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path              = "/opt/tagnos/dev/terraform.tfstate"
  }
}

module "sg-groups" {
  source                    = "../modules/security-group"
  name                      = "public-livedemo-prod-wildfly-sg"
  description               = "Secuirty Group for public-livedemo-prod-wildfly server"
  vpc_id                    = data.terraform_remote_state.vpc.outputs.vpc_id
  ingress_cidr_blocks       = ["0.0.0.0/0"]
  ingress_ipv6_cidr_blocks  = ["::/0"]
  ingress_rules             = ["http-80-tcp", "http-8080-tcp", "ssh-tcp"]

  ingress_with_cidr_blocks = [
    {
      from_port   = 7600
      to_port     = 7600
      protocol    = "tcp"
      description = "Jgroup TCP port"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 5701
      to_port     = 5710
      protocol    = "tcp"
      description = "Hazelcastports"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 9990
      to_port     = 9990
      protocol    = "tcp"
      description = "admin console"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 4188
      to_port     = 4188
      protocol    = "tcp"
      description = "Mysql DB port"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  ingress_with_ipv6_cidr_blocks = [
    {
      from_port        = 9990
      to_port          = 9990
      protocol         = "tcp"
      description      = "Admin Console"
      ipv6_cidr_blocks = "::/0"
    },
  ]

  egress_rules              = ["all-all"]

  tags = {
    Owner                   = "Tagnos"
  }
}

data "aws_ami" "WildflyRabbitMQSmokeImage" {
  most_recent = true
  owners = ["self"]

  filter {
    name   = "name"
    values = ["WildflyRabbitMQSmokeImage*"]
  }
}

module "ec2" {
  source = "../modules/ec2-instance"
  name                        = "public-livedemo-prod-wildfly"
  ami                         = data.aws_ami.WildflyRabbitMQSmokeImage.id
  instance_type               = var.instance_type
  subnet_id                   = tolist(data.terraform_remote_state.vpc.outputs.private_subnet_ids)[0]
  vpc_security_group_ids      = [module.sg-groups.this_security_group_id]
  associate_public_ip_address = false
  key_name                    = var.key_pair

  tags = {
    Owner = "Tagnos"
  }
}