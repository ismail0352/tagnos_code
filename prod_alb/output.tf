output "associated_instance_in_lb" {
  value = aws_lb_target_group_attachment.prod_wildfly.*.target_id
}

output "alb_arn" {
  value = module.alb.this_lb_arn
}

output "target_group_alb_arn" {
  value = module.alb.target_group_arns
}
