provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "tagnos"
}

module "backend_terraform_state" {
  source  = "../modules/backend_terraform_state"
  key     = "global/s3/prod/vpc/terraform.tfstate"
  region  = var.region
}

//data "aws_acm_certificate" "tagnos" {
//  domain = "*.tagnos.com"
//}

module "alb_security_group" {
  source  = "../modules/security-group"

  name        = "alb-sg"
  description = "Security group for Prod ALB"
  vpc_id      = module.backend_terraform_state.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "https-443-tcp"]
  egress_rules        = ["all-all"]
}

##################################################################
# Application Load Balancer
##################################################################
module "alb" {
  source = "../modules/alb"

  name = "prod-alb"

  load_balancer_type = "application"

  vpc_id          = module.backend_terraform_state.vpc_id
  security_groups = [module.alb_security_group.this_security_group_id]
  subnets         = module.backend_terraform_state.public_subnet_ids

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    },
  ]

//  https_listeners = [
//    {
//      port               = 443
//      protocol           = "HTTPS"
//      certificate_arn    = data.aws_acm_certificate.tagnos.arn
//      target_group_index = 1
//    },
//  ]

  target_groups = [
    {
      name                 = "target-group-prod-alb"
      backend_protocol     = "HTTP"
      backend_port         = 8080
      target_type          = "instance"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/healthz"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 3
        timeout             = 6
        protocol            = "HTTP"
        matcher             = "200-399"
      }
    }
  ]

  tags = {
    Owner = "Tagnos"
    Environment = "Prod"
  }
}

data "terraform_remote_state" "instance" {
  backend             = "s3"
  config = {
    bucket            = "tagnos-terraform-backend-state"
    key               = "global/s3/prod/instance/terraform.tfstate"
    region            = var.region
    profile           = "tagnos" //Without this it tries to connect to default
  }
}

resource "aws_lb_target_group_attachment" "prod_wildfly" {
  count = length(data.terraform_remote_state.instance.outputs.instance_id)
  target_group_arn = module.alb.target_group_arns[0]
  target_id = element(data.terraform_remote_state.instance.outputs.instance_id, count.index)
}
