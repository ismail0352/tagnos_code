data "terraform_remote_state" "vpc" {
  backend             = "s3"
  config = {
    bucket            = "tagnos-terraform-backend-state"
    key               = var.key // "global/s3/prod/vpc/terraform.tfstate"
    region            = var.region
    profile           = "tagnos" //Without this it tries to connect to default
  }
}
