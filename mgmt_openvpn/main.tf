provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "tagnos"
}

module "backend_terraform_state" {
  source  = "../modules/backend_terraform_state"
  key     = "global/s3/mgmt/vpc/terraform.tfstate"
  region  = var.region
}

module "mgmt-openvpn-sg" {
  source                    = "../modules/security-group"
  name                      = "mgmt-openvpn-sg"
  description               = "Secuirty Group for openvpn server"
  vpc_id                    = module.backend_terraform_state.vpc_id
  ingress_cidr_blocks       = ["0.0.0.0/0"]
  ingress_ipv6_cidr_blocks  = ["::/0"]
  ingress_rules             = ["openvpn-udp", "openvpn-tcp", "openvpn-https-tcp", "ssh-tcp"]
  egress_rules              = ["all-all"]

  tags = {
    Owner              = "Tagnos"
  }
}

data "aws_ami" "ubuntu18" {
  most_recent = true
  owners = [var.ubuntu_account_number]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}

data "template_file" "openvpn" {
  template = file("../global/scripts/create-vpn.sh")
}

module "ec2" {
  source = "../modules/ec2-instance"
  instance_count              = 1
  name                        = "mgmt-openvpnserver"
  ami                         = data.aws_ami.ubuntu18.id
  instance_type               = var.instance_type
  subnet_id                   = tolist(module.backend_terraform_state.public_subnet_ids)[0]
  //  private_ips                 = ["172.31.32.5", "172.31.46.20"]
  vpc_security_group_ids      = [module.mgmt-openvpn-sg.this_security_group_id]
  associate_public_ip_address = true
  user_data                   = data.template_file.openvpn.rendered
  key_name                    = var.key_pair

  tags = {
    Owner = "Tagnos"
  }
}


//This block only runs for the first time, in the second run IT WILL RECREATE INSTANCE and file will NOT BE PRESENT
resource "null_resource" "mgmt-openvpnserver" {
  connection {
    type        = "ssh"
    user        = var.ssh_user
    private_key = file("../../${var.key_pair}.pem")
    host        = element(module.ec2.public_ip, 0 )
  }

  provisioner "file" {
    source      = var.source_path
    destination = var.destination_path
    //    on_failure  = "continue"
  }

  //  To change permission
  provisioner "remote-exec" {
     inline = [
      "chmod +x ${var.destination_path}",
    ]
  }
}
